<?php namespace texcanarias\phpunit;

use PHPUnit\Framework\TestCase;
use texcanarias\ZipValidator\ZipCodeValidator;

final class MainTest extends TestCase {

    public function testES() {
        $zv = ZipCodeValidator::factory();
        $this->assertTrue( $zv->validate('35012', 'es') );
        $this->assertTrue( $zv->validate('35012', 'ES') );
        $this->assertFalse( $zv->validate('p35012', 'ES'));
    }

}
